﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace DocuHLCore 
{ 
    public class DocuHLUtility
    {
        #region Public Events

        /// <summary>
        /// Data transfer progress event.
        /// </summary>
        public event EventHandler<TransferProgressEventArgsEx> TransferProgressEx;

        /// <summary>
        /// Data transfer complete event.
        /// </summary>
        public event EventHandler<TransferCompleteEventArgsEx> TransferCompleteEx;

        /// <summary>
        /// Error event.
        /// </summary>
        public event EventHandler<ErrorEventArgs> ErrorEvent;

        #endregion
        /// <summary>
        /// Function for Encripting the file
        /// </summary>
        /// <param name="sFileName">File to be encripted.</param>
        /// <param name="sEncryptPasswrd">Encription password.</param>
        /// <param name="sPGPKeyFile">PGP key file.</param>

        private void EncriptFile(string sFileName, string sEncryptPasswrd, string sPGPKeyFile)
        {

        }

        /// <summary>
        /// Function for ziping the files in the directory
        /// </summary>
        /// <param name="sDirName">Path of directory to be zipped.</param>
        /// <param name="sZipFileName">return the file name after zipping.</param>
        private void ZipFile(string sDirName, ref string sZipFileName)
        {
        }

        /// <summary>
        /// Function that helps to upload files
        /// </summary>
        /// <param name="sLocalFile">Path of local file to be uploaded.</param>
        /// <param name="sRemoteLocation">Path of remote location to which upload is to be done.</param>
        /// <param name="sFtpHost">Host Name.</param>
        /// <param name="sFtpPort">Port Number.</param>
        /// <param name="sUserName">User name for the site.</param>
        /// <param name="sPassword">Password for the site.</param>
        private void PutFile(string sLocalFile, string sRemoteLocation, string sFtpHost, string sFtpPort, string sUserName, string sPassword)
        {

        }



        /// <summary>
        /// Function for ziping the files in the directory
        /// </summary>
        /// <param name="sDirName">Path of directory which contains images to be uploaded.</param>
        public void UploadFile(string sDirName)
        {
 
        }

    }
}
