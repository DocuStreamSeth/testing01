﻿using System;
namespace DocuHLCore
{

    /// <summary>
    /// Event arguments to facilitate the transfer complete event.
    /// </summary>
    /// 
    public class TransferCompleteEventArgsEx : EventArgs
    {

    }

    /// <summary>
    /// Event arguments to facilitate the transfer progress event.
    /// </summary>
    public class TransferProgressEventArgsEx : EventArgs
    {

    }

   /// <summary>
    /// Event arguments to facilitate the error event.
    /// </summary>
    public class ErrorEventArgs : EventArgs
    {

    }
}